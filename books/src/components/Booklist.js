import {useState} from 'react';
import Book from ".Book";
const Booklist = () => {

  const [books, setBooks] = useState();
  <section>
  <Book
    title="Van Roy naar Royalistiq"
    author="Roy"
    image="./images/reactimage1.jpg"
  />
  <Book
    title="Mijn meningen zijn feiten"
    author="Thierry Baudet"
    image="./images/reactimage2.jpg"
  />
  <Book
    title="Rich Dad Poor Dad"
    author="Robert T. Kiyosaki"
    image="./images/reactimage3.jpg"
  />
</section>


  return (
    <section className='container'>
      {books.map((book) =>(
<Book title={book.title} author={book.author} image={book.image}/>
      ))
      };
    </section>
  );
};

export default Booklist;
