import logo from './logo.svg';
import './App.css';

function App() {

  const [count, setCount] = useState(0);

  const increamentHandler = () => {
  setCount(count + 1)
  }
  const decreamentHandler = () => {
    setCount(count - 1)
  }
  return (
    <div>
      <button onClick={decreamentHandler}>-</button>
      <span>{count}</span>
      <button onClick={increamentHandler}>+
      </button>
    </div>
  );
}

export default App;
